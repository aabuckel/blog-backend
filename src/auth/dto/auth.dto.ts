import { IsEmail, MinLength, MaxLength, Matches } from 'class-validator';

export class AuthCredentialsDto {
  @IsEmail()
  email: string;

  @MinLength(12)
  @MaxLength(16)
  @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/)
  password: string;
}