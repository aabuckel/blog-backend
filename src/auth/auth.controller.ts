import { Controller, Post, Body, ValidationPipe } from '@nestjs/common';
import { AuthCredentialsDto } from './dto/auth.dto';
import { AuthService } from './auth.service';
import { CreateUserDto } from '../users/dto/createUser.dto';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
  ) {

  }

  @Post('/signup')
  signup(@Body(ValidationPipe) createUserDto: CreateUserDto): Promise<any> {
    return this.authService.signup(createUserDto);
  }

  @Post('/signin')
  signin(@Body(ValidationPipe) authCredentialsDto: AuthCredentialsDto): Promise<object> {
    return this.authService.login(authCredentialsDto);
  }
}