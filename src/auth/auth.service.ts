
import { JwtService } from '@nestjs/jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtPayload } from './jwt-payload.interface';
import { AuthCredentialsDto } from './dto/auth.dto';
import { CreateUserDto } from '../users/dto/createUser.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) { }

  async signup(createUserDto: CreateUserDto): Promise<any> {
    return this.usersService.createUser(createUserDto);
  }

  async login(authCredentialsDto: AuthCredentialsDto): Promise<any | { status: number }> {
    const user = await this.usersService.validate(authCredentialsDto);
    const { email } = authCredentialsDto;
    if (!user) {
      throw new UnauthorizedException('Wrong email or password');
    }
    const payload: JwtPayload = { email };
    const accessToken = this.jwtService.sign(payload);

    return {
      expires_in: 3600,
      access_token: accessToken,
      user_name: payload,
      status: 200,
    };

  }
}
