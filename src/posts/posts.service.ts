import { Injectable, NotFoundException, UnauthorizedException, HttpModule } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PostRepository } from './post.repository';
import { CreatePostDto } from './dto/createPost.dto';
import { Posts } from './entities/post.entity';
import { User } from 'src/users/user.entity';

@Injectable()
export class PostsService {
  constructor(
    @InjectRepository(PostRepository)
    private readonly postRepository: PostRepository,
  ) { }

  async getPosts(user: User): Promise<Posts[]> {
    return await this.postRepository.getPosts(user);
  }

  async getPost(
    id: number,
    user: User,
  ): Promise<Posts> {
    const post = await this.postRepository.findOne({ where: { id, userId: user.id } });

    if (!post) {
      throw new NotFoundException('This post does not exist');
    }
    return post;
  }

  async createPost(createPostDto: CreatePostDto, user: User): Promise<Posts> {
    return this.postRepository.createPost(createPostDto, user);
  }

  async updatePost(id: number, createPostDto: CreatePostDto, user: User): Promise<Posts> {
    try {
      const post = await this.postRepository.findOne({ where: { id, userId: user.id } });

      if (!post) {
        throw new NotFoundException('This post does not exist');
      }
      await this.postRepository.update({ id, userId: user.id }, createPostDto);
      return post;
    } catch (error) {
      throw new UnauthorizedException('You dont have access to see this page');
    }
  }

  async deletePost(id: number, user: User): Promise<string> {
    const query = await this.postRepository.delete({ id, userId: user.id });
    if (!query) {
      throw new NotFoundException('This post does not exist');
    }
    return 'Post deleted.';
  }
}
