import { Controller, Post, Body, ValidationPipe, UsePipes, Get, Param, ParseIntPipe, Put, UseGuards, Delete } from '@nestjs/common';
import { PostsService } from './posts.service';
import { CreatePostDto } from './dto/createPost.dto';
import { Posts } from './entities/post.entity';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { GetUser } from 'src/shared/decorators/get-user.decorator';
import { AuthGuard } from '@nestjs/passport';
import { User } from 'src/users/user.entity';

@UseGuards(AuthGuard())
@ApiUseTags('Posts')
@ApiBearerAuth()
@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) { }

  @Get()
  findAll(@GetUser() user: User): Promise<Posts[]> {
    return this.postsService.getPosts(user);
  }

  @Get('/:id')
  findOne(
    @Param('id', ParseIntPipe) id: number,
    @GetUser() user: User): Promise<Posts> {
    return this.postsService.getPost(id, user);
  }

  @Post()
  @UsePipes(ValidationPipe)
  create(
    @Body() createPostDto: CreatePostDto,
    @GetUser() user: User,
  ): Promise<Posts> {
    return this.postsService.createPost(createPostDto, user);
  }

  @Put('/:id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() createPostDto: CreatePostDto,
    @GetUser() user: User,
  ): Promise<Posts> {
    return this.postsService.updatePost(id, createPostDto, user);
  }

  @Delete('/:id')
  delete(
    @Param('id', ParseIntPipe) id: number,
    @GetUser() user: User,
  ): Promise<string> {
    return this.postsService.deletePost(id, user);
  }
}
