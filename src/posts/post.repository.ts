import { EntityRepository, Repository } from 'typeorm';
import { Posts } from './entities/post.entity';
import { CreatePostDto } from './dto/createPost.dto';
import { HttpException, HttpStatus } from '@nestjs/common';
import { User } from 'src/users/user.entity';

@EntityRepository(Posts)
export class PostRepository extends Repository<Posts> {

  async getPosts(user: User): Promise<Posts[]> {
    const query = this.createQueryBuilder('posts');
    query.where('posts.userId = :userId', { userId: user.id });

    const posts = await query.getMany();
    return posts;
  }

  async createPost(createPostDto: CreatePostDto, user: User): Promise<Posts> {
    const { title, description, imageUrl, regulationType, sectionId } = createPostDto;
    try {
      const post = new Posts();
      post.title = title;
      post.description = description;
      post.imageUrl = imageUrl;
      post.regulationType = regulationType;
      post.user = user;
      post.sectionId = sectionId;
      await post.save();
      delete post.user;
      if (!post) {
        throw new HttpException({
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: 'We couldnt process your request.',
        }, 500);

      }
      return post;
    } catch (error) {
      throw error;
    }
  }
}