import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column, CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';
import { User } from 'src/users/user.entity';
import { Section } from 'src/sections/section.entity';

@Entity()
export class Posts extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  imageUrl: string;

  @Column()
  regulationType: string;

  @CreateDateColumn()
  createdAt: Date;

  @Column()
  deletedAt: Date;

  @UpdateDateColumn()
  modifiedAt: Date;

  @ManyToOne(type => User, user => user.posts, { eager: false })
  user: User;

  @Column()
  userId: number;

  @ManyToOne(type => Section, section => section.posts, { eager: false })
  section: Section;

  @Column()
  sectionId: number;
}
