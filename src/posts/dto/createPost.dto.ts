import { IsNotEmpty, IsString, MinLength, MaxLength, IsUrl } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreatePostDto {
  @ApiModelProperty({})
  @IsString()
  @MinLength(10)
  @MaxLength(50)
  title: string;

  @ApiModelProperty()
  @IsString()
  @MaxLength(100)
  description: string;

  @ApiModelProperty()
  @IsNotEmpty()
  @IsUrl()
  imageUrl: string;

  @ApiModelProperty()
  @IsNotEmpty()
  @IsString()
  regulationType: string;

  @ApiModelProperty()
  @IsNotEmpty()
  sectionId: number;
}