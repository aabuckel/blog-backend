import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());

  const options = new DocumentBuilder()
    .setBasePath('api/v1')
    .setTitle('9GAG')
    .setDescription('This docs describes the 9GAG API')
    .addBearerAuth()
    .setVersion('1.0')
    .addTag('blog')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  app.setGlobalPrefix('api/v1');
  SwaggerModule.setup('docs/api', app, document);
  await app.listen(3000);
}
bootstrap();
