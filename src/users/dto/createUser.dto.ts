import { IsNotEmpty, IsString, MinLength, MaxLength, IsEmail, Matches, IsUrl, IsDate } from "class-validator";
import { ApiModelProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @ApiModelProperty({
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  readonly name: string;

  @ApiModelProperty({
    type: 'string',
    minimum: 5,
    maximum: 10,
  })
  @IsString()
  @MinLength(5)
  @MaxLength(10)
  readonly userName: string;

  @ApiModelProperty()
  @IsNotEmpty()
  @IsEmail()
  readonly email: string;

  @ApiModelProperty({
    minimum: 12,
    maximum: 16,
    description: `Password between 12 and 16 characters, 
                  must contain at least one lowercase letter, 
                  one uppercase letter, one numeric digit, 
                  and one special character, but cannot contain 
                  whitespace.`,
  })
  @MinLength(12)
  @MaxLength(16)
  @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/)
  readonly password: string;
}