import { IsNotEmpty, IsString, MinLength, MaxLength, IsEmail, Matches, IsUrl, IsDate } from "class-validator";
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateUserDto {
  @ApiModelProperty()
  @IsNotEmpty()
  @IsString()
  readonly name: string;

  @ApiModelProperty()
  @IsString()
  @MinLength(5)
  @MaxLength(10)
  readonly userName: string;

  @ApiModelProperty()
  @IsNotEmpty()
  @IsEmail()
  readonly email: string;

  @ApiModelProperty()
  @MinLength(12)
  @MaxLength(16)
  @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/)
  readonly password: string;

  @ApiModelProperty()
  @MaxLength(40)
  readonly aboutMe: string;

  @ApiModelProperty()
  readonly gender: string;

  @ApiModelProperty()
  @IsUrl()
  readonly profilePicture: string;

  @ApiModelProperty()
  readonly birthday: string;
}