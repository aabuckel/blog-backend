import { Injectable, NotFoundException } from '@nestjs/common';
import { UserRepository } from './user.respository';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { UpdateUserDto } from './dto/updateUser.dto';
import { CreateUserDto } from './dto/createUser.dto';
import { AuthCredentialsDto } from '../auth/dto/auth.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: UserRepository,
  ) { }

  async findAll(): Promise<User[]> {
    return await this.userRepository.find();
  }

  async findUser(id: number): Promise<User> {
    const user = await this.userRepository.findOne(id);

    if (!user) {
      throw new NotFoundException(`User does not exist`);
    }

    return user;

  }

  async findByEmail(email: string): Promise<User> {
    return this.userRepository.findOne({ email });
  }

  async validate(authCredentialsDto: AuthCredentialsDto) {
    return await this.userRepository.validatePassword(authCredentialsDto);
  }

  async createUser(createUserDto: CreateUserDto): Promise<void> {
    return this.userRepository.createUser(createUserDto);
  }

  async updateUser(updateUserDto: UpdateUserDto, id: number): Promise<User> {
    try {
      const userToUpdate = await this.userRepository.findOne(id);
      if (!userToUpdate) {
        throw new NotFoundException(`section with the ${id} not found`);
      }
      const userUpdated = Object.assign(userToUpdate, updateUserDto);
      return await this.userRepository.save(userUpdated);
    } catch (error) {
      throw error;
    }
  }

  async deleteUser(id: number): Promise<string> {
    try {
      const user = await this.userRepository.findOne(id);
      this.userRepository.delete(id);
      if (!user) {
        throw new NotFoundException(`User does not exist`);
      }
      return 'User was successfully deleted.';

    } catch (error) {
      throw error;
    }

  }
}
