import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Unique,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  VersionColumn,
  OneToMany,
} from 'typeorm';
import * as argon2 from 'argon2';
import { Posts } from '../posts/entities/post.entity';

@Entity()
@Unique(['email', 'userName'])
export class User extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  userName: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column({ type: 'boolean', default: false })
  active: boolean;

  @Column()
  aboutMe: string;

  @Column()
  gender: string;

  @Column()
  profilePicture: string;

  @Column()
  birthday: string;

  @OneToMany(type => Posts, posts => posts.user, { eager: true })
  posts: Posts[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @VersionColumn()
  savedTimes: number;

  async validatePassword(password: string): Promise<boolean> {
    const hash = await argon2.hash(password);
    return hash === this.password;
  }
}