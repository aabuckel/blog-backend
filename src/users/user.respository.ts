import { Repository, EntityRepository } from 'typeorm';
import * as argon2 from 'argon2';
import { User } from './user.entity';
import { HttpException, HttpStatus } from '@nestjs/common';
import { CreateUserDto } from './dto/createUser.dto';
import { AuthCredentialsDto } from '../auth/dto/auth.dto';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async createUser(createUserDto: CreateUserDto): Promise<any> {
    const user = new User();
    const { name, userName, email, password } = createUserDto;
    const userExists = await this.findOne({ userName, email });

    if (userExists) {
      throw new HttpException('This username already exists, please take another one', HttpStatus.CONFLICT);
    }
    try {
      user.name = name;
      user.userName = userName;
      user.email = email;
      user.password = await argon2.hash(password);
      await user.save();
      return user;
    } catch (error) {
      throw new HttpException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        error: 'The user couldnt be saved.',
      }, 500);

    }
  }

  async validatePassword(authCredentialsDto: AuthCredentialsDto) {
    const { email, password } = authCredentialsDto;
    const user = await this.findOne({ email });
    const isPasswordValid = user.validatePassword(password);
    if (user && isPasswordValid) {
      return email;
    } else {
      return null;
    }
  }
}