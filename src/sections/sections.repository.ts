import { Repository, EntityRepository } from 'typeorm';
import { Section } from './section.entity';
import { CreateSectionDto } from './dto/createSection.dto';
import { ConflictException } from '@nestjs/common';

@EntityRepository(Section)
export class SectionsRepository extends Repository<Section> {
  async createSection(createSectionDto: CreateSectionDto): Promise<Section> {
    const section = new Section();
    const { name, slogan, imageUrl } = createSectionDto;

    section.name = name;
    section.slogan = slogan;
    section.imageUrl = imageUrl;

    try {
      await section.save();
    } catch (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        throw new ConflictException('Section name already exist');
      }
    }

    return section;
  }
}
