import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  VersionColumn,
  Unique,
  OneToMany,
} from 'typeorm';
import { Posts } from 'src/posts/entities/post.entity';

@Entity()
@Unique(['name'])
export class Section extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar', { length: 100 })
  name: string;

  @Column('varchar', { length: 200 })
  slogan: string;

  @Column('varchar', { length: 300 })
  imageUrl: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: null;

  @VersionColumn()
  savedTimes: Date;

  @Column('timestamp')
  deletedAt: null;

  @OneToMany(type => Posts, posts => posts.section, { eager: true })
  posts: Posts[];
}
