import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SectionsRepository } from './sections.repository';
import { CreateSectionDto } from './dto/createSection.dto';
import { Section } from './section.entity';

@Injectable()
export class SectionsService {
  constructor(
    @InjectRepository(SectionsRepository)
    private sectionsRepository: SectionsRepository,
  ) {}

  async getSections(): Promise<Section[]> {
    return await this.sectionsRepository.find();
  }

  async getSection(id: number) {
    const section = await this.sectionsRepository.findOne(id);

    if (!section) {
      throw new NotFoundException(`section with the ${id} not found`);
    }

    return section;
  }

  async createSection(createSectionDto: CreateSectionDto): Promise<Section> {
    return await this.sectionsRepository.createSection(createSectionDto);
  }

  async deleteSection(id: number): Promise<void> {
    try {
      const query = await this.sectionsRepository.delete(id);
      if (query.affected === 0) {
        throw new NotFoundException(`section with the ${id} not found`);
      }
    } catch (error) {
      throw error;
    }
  }

  async updateSection(
    id: number,
    createSectionDto: CreateSectionDto,
  ): Promise<Section> {
    try {
      const section = await this.sectionsRepository.findOne(id);
      if (!section) {
        throw new NotFoundException(`section with the ${id} not found`);
      }
      await this.sectionsRepository.update({ id }, createSectionDto);
      return section;
    } catch (error) {
      throw error;
    }
  }
}
