import {
  Controller,
  Get,
  Param,
  Post,
  UsePipes,
  ValidationPipe,
  Body,
  Delete,
  ParseIntPipe,
  Put,
  UseGuards,
} from '@nestjs/common';
import { SectionsService } from './sections.service';
import { Section } from './section.entity';
import { CreateSectionDto } from './dto/createSection.dto';
import { ApiUseTags, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

@UseGuards(AuthGuard())
@ApiUseTags('Sections')
@ApiBearerAuth()
@Controller('sections')
export class SectionsController {
  constructor(private sectionsService: SectionsService) { }

  @Get()
  @ApiResponse({
    status: 200,
    description: 'Returns sections.',
  })
  getSections(): Promise<Section[]> {
    return this.sectionsService.getSections();
  }

  @Get('/:id')
  @ApiResponse({
    status: 200,
    description: 'Return a single section.',
  })
  getSectionById(@Param('id') id: number): Promise<Section> {
    return this.sectionsService.getSection(id);
  }

  @Post()
  @ApiResponse({
    status: 201,
    description: 'Creates a section.',
  })
  @UsePipes(ValidationPipe)
  createSection(@Body() createSectionDto: CreateSectionDto): Promise<Section> {
    return this.sectionsService.createSection(createSectionDto);
  }

  @Delete('/:id')
  @ApiResponse({
    status: 204,
    description: 'Deletes a Section.',
  })
  deleteSection(@Param('id', ParseIntPipe) id: number): void {
    this.sectionsService.deleteSection(id);
  }

  @Put('/:id')
  @ApiResponse({
    status: 204,
    description: 'Updates a Section by',
  })
  updateSection(
    @Param('id', ParseIntPipe) id: number,
    @Body() createSectionDto: CreateSectionDto,
  ): Promise<Section> {
    return this.sectionsService.updateSection(id, createSectionDto);
  }
}
