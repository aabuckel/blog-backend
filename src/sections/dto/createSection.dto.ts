import { validate, IsNotEmpty, IsString, IsUrl } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
export class CreateSectionDto {
  @ApiModelProperty()
  @IsNotEmpty()
  @IsString()
  readonly name: string;

  @ApiModelProperty()
  @IsNotEmpty()
  @IsString()
  readonly slogan: string;

  @ApiModelProperty()
  @IsNotEmpty()
  @IsString()
  @IsUrl()
  readonly imageUrl: string;
}
